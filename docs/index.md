# My coding style

This is memorandum of the programing languages conding style.

## Description cases

| Description | Example      | &emsp; | &emsp; | &emsp; | &emsp; |
|-------------|--------------|--------|--------|--------|--------|
| Camel case  | doSomething  | &emsp; | &emsp; | &emsp; | &emsp; |
| Pascal case | DoSomething  | &emsp; | &emsp; | &emsp; | &emsp; |
| Snake case  | do_something | &emsp; | &emsp; | &emsp; | &emsp; |

