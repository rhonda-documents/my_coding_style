## My c/c++ conding rule 
Basically follow the [Google coding style](https://google.github.io/styleguide/cppguide.html).

----------------------------------------------------------------------------

## Naming

### General
Names should be descriptive; avoid abbreviation.

### Files
Use **Pascal case** and end in `.cc` or `.hh`.

`MyClassFile.cc`, `MyClassFile.hh` 

!!! Note
    This is different from the Google style.

### Type names
Use **Pascal case**.  
`MyClass`, `MyDataCont`.

### Variable names
Use **snake name**. Data members in classes (but not in struct) accompany an additional underscore at end.

```c++
int num_of_event = 0;
std::string detector_name = "BH1";

class MyClass
{
  int an_member_;
}

struct MyStruct
{
  int an_member;
}
```

### Constant names
Variables declared by `constexpr` or `const`, and in `enum`, should be named with a leading ***k*** followed by **Pascal case**.

```c++
const int kNumOfDetectors = 10;
enum DetectorIndex {kBH1, kBH2};
```

### Function names
Use **Pascal case**. In the case of *Getter* and *Setter*, **snake case** can be used.  

```c++
void AddHit(int ADC, int TDC);
void ClearHits();
```


### Macro names
C language macros are named with all capitals and underscores.

```c++
define ENABLE_DEBUG 1
```

----------------------------------------------------------------------------

## C/C++ features
### Integer types
Use the precise-width integer types defined in `stdin.h`, such as `uint32_t` instead of buit-in C language inter types.

----------------------------------------------------------------------------

## Code style
### Space vs. Tab
Use always spaces, and indent 2 spaces at the time.

### Conditionals
Use the following expression.
**Do not** use conditional keywords, `if` and `else` (and also loop expressions, `for` and `while`), without braces.

```c++
if(true
    && condition_A == A
    && condition_B == B
    ){
  doSomething
}else if(condition_C == C) {
  doSomething...
}else{ 
  doSomething...
}

//*** Bad case ***//
if (condition_A)
  doSomething...
```

!!! Note
    It's different from Google style.

### Class format
Sections in `public`, `protected`, and `private` order, and indent one space. 
List the class members following order: types, constant, factory functions, constructor, destructor, data members, methods, and operators.

```c++
MyClass : public Parent {
 public:
  
  MyClass();
  ~MyClass();
  
  AddData(MyData );
  ClearData();

 private:
  using DataArray = std::vector<MyData>;
  const int kNumOfDetectors {10};
  
  MyData data_bh1_; 
};
```
