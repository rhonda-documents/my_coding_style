## My VHDL conding rule 
This coding style is self-directed and does not belong to any global standard.

----------------------------------------------------------------------------

## Code style

### General

* Do not use magic number as much as possible.
* Separate *definition* vhdl files, which include type, subtype, record, and constant, and *implementation* vhdl files.
* An implementation vhdl file can accompany a definition file having the same name with a leading ***def*** keyword.
    - e.g., `MyModule.vhd` and `defMyModule.vhd`.
* Use `record` for a specific control signals such as the trigger.
* Separate physical, timing, and implementation constraints `xdc` files.
* Toplevel vhdl file should be `toplevel.vhd`.

### File path

    my_project.srcs
     ├── constrs_1
     │   ├── pins.xdc
     │   ├── timing.xdc
     │   └── impl.xdc
     │
     └── source_1
         ├── definition
         └── implementation

----------------------------------------------------------------------------

## Naming

### General
Words in attribute, sub-attribute, and sub-sub-attribute order, or in verb + objective order.

```VHDL
  signal clk_mmcm_100MHz : std_logic;
  signal reset_something : std_logic;
  signal L1trig_from_hrm : std_logic;
  signal reg_something   : std_logic;
```

### Port names
Use **Camel case** except for the top-level ports; they are named using **all capitals and underscores.** Module ports, which are connected to the top-level ports directly, should be named using all capitals and undersores in order to show those ports correspond to the top-level ports.

```VHDL
-- Top-level ports
  CLK_OSC_50MHz         : in std_logic;

-- Module ports
  clkOsc50MHz           : in std_logic;
```

### Signal names
Use **snake case**.

### Type names
Use **Pascal case**.

### Constant names
Values declared by `constant` should be named with a leading ***k*** followed by **Pascal case**.

```VHDL
constant kWidthHBCounter :positive:=16;
```

